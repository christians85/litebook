<?php

namespace Litebook\Views;

// falls die Konstanten noch nicht geladen sind
require_once(__DIR__ . "/../configs/CONSTANTS.php");

/**
 * Simple Template-Engine with two functions:
 *  One to place a single template with contents to the page. 
 *  The other loops over a 2-dimensional Array to repeat printing.
 */
class Templates {


    /**
     * Simple function to place templates inside the website.
     * @param string $template      Template Name without path. If template is in subfolder, use / to seperate.
     * @param array $data           Array with data to place in Template.
     */
    public static function place(string $template, array $data) {

        str_replace(".php", "", $template);
        // Takes the Template-String and resolves it into a usable path:
        $path = __ROOT__ . DS . "templates" . DS . $template . ".php";
        
        // Checks if template path even exists.
        if(!is_file($path)) {
            trigger_error("Das Template unter $path konnte nicht geladen werden oder existiert nicht.");
            return null;
        }

        // extract everything from array
        extract($data, EXTR_SKIP);

        // takes all following into the buffer and returns it
        ob_start();
        include($path);    // here the template is loaded, all data is placed into it
        return ob_flush();
    }


    /**
     * Calls the place function for every node in array
     * @param string $template      Template Name without path. If template is in subfolder, use / to seperate
     * @param array $data           2-dimensional array with data to place in template
     */
    public static function placeLoop(string $template, array $data) {
        foreach ($data as $node) {
            Templates::place($template, $node);
        }
    }

}