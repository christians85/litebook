<?php

namespace Litebook\Models;

use PDO;

/*
 *  This Class encapsulates all Database-Related methods to
 *  provide the data needed to run the Guestbook.
 * 
 *   @TODO Maybe make this singleton again.
 */

class EntryModel
{

    // PDO-Instance
    private $db;

    // For the Functions which fetch Data from DB
    public const DESC = 1;
    public const ASC = 0;

    /**
     * Constructor of this Class. Creates a empty initial array of entries.
     * @param PDO $db PDO-DB-Handler
     */
    function __construct(PDO $db)
    {
        $this->db = $db;
    }


    /**
     * Returns all entries of the guestbook.
     * 
     * @param int $direction 0 or nothing for descending (default), 1 for ascending.
     * 
     * @return array Array of results.
     */
    public function allFromDB(int $direction = DESC): array
    {

        // Select all entries based on direction
        $sql  = "SELECT id, name, time, homepage, email, message, (SELECT COUNT(id) FROM entry) AS numb";
        $sql .= " FROM entry";
        if ($direction == DESC) {
            $sql .= " ORDER BY id DESC";
        }

        // Preparation of SQL Statement
        $stmt = $this->db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Creates a WHERE ... AND ... SQL Statement from a Array of Conditions
     * given as a string.
     * 
     * @param array $conditions Array of conditions
     * 
     * @return string WHERE ... AND ... statement in SQL Syntax
     */
    public function buildCondString(array $conditions): string
    {
        $condition_string = "";
        foreach ($conditions as $condition) {

            // only if the string is empty: add WHERE prefix
            if ($condition_string == "") {
                $condition_string = " WHERE";
            }

            // if the string doesn't contain only WHERE -> there must be an AND to combine conditions
            if ($condition_string != " WHERE") {
                $condition_string .= " AND ";
            }
            $condition_string .= " " . $condition;
        }

        return $condition_string;
    }


    /**
     * Fetches a number of entries from the Database based on
     * a offset, a number of entries, a sorting direction and
     * finally a optional set of conditions - returns this as an array.
     * 
     * @param int $offset Offset of skipped results
     * @param int $number Number of results
     * @param int $direction 0 for descending, 1 for ascending
     * @param array $conditions Optional: Conditions for the SQL Statement as Array of Strings
     * 
     * @return array Array of results.
     */
    public function fromDBWithCondition(
        int $offset,
        int $number,
        int $direction = DESC,
        array $conditions = []
    ): array {

        // Builds a condition string if a entry-param was given.
        $condition_string = "";
        if (sizeof($conditions) > 0) {
            $condition_string = $this->buildCondString($conditions);
        }

        // SQL Statement
        $sql  = "SELECT id, name, time, homepage, email, message, (SELECT COUNT(id) FROM entry) AS numb";
        $sql .= " FROM entry";
        $sql .= $condition_string;
        if ($direction == DESC) {
            $sql .= " ORDER BY id DESC";
        }
        $sql .= " LIMIT :off, :num";

        // Prepares and binds limits.
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':off', $offset, PDO::PARAM_INT);
        $stmt->bindParam(':num', $number, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * Adds an entry to the Database table entry
     * 
     * @param string $name Name of Visitor
     * @param string $email Email adress of visitor
     * @param mixed $homepage (null or string) Homepage-URL of visitor
     * @param string $message The Message of the entry.
     * 
     * @return bool false if not successful, true if sucessful
     */
    public function addEntry(
        string $name, 
        string $email, 
        $homepage, 
        string $message
    ): bool {

        // SQL Statement to insert an guestbook-entry to the Database
        $sql  = "INSERT INTO entry (name, email, homepage, message)";
        $sql .= " VALUES (:na, :em, :hp, :msg)";

        // Preparation and Execution
        $stmt = $this->db->prepare($sql);

        return $stmt->execute(array(
            'na' => $name,
            'em' => $email,
            'hp' => $homepage,
            'msg' => $message
        ));
    }


    /**
     * Edits an entry of this guestbook.
     * 
     * @param int $id ID of the post to select the right one in Database.
     * @param string $name Name of Visitor
     * @param string $email Email adress of visitor
     * @param mixed $homepage (null or string) Homepage-URL of visitor
     * @param string $message The Message of the entry.
     * 
     * @return bool false if not successful, true if sucessful
     */
    public function editEntry(
        int $id, 
        string $name, 
        string $email, 
        $homepage, 
        string $message
    ): bool {

        $sql  = "UPDATE entry SET name = :na, email = :em,";
        $sql .= " homepage = :hp, message = :msg WHERE id = :id";

        $stmt = $this->db->prepare($sql);
        
        $stmt->execute(array(
            'na' => $name,
            'em' => $email,
            'hp' => $homepage,
            'msg' => $message,
            'id' => $id
        ));

        return $stmt->execute();
    }


    /**
     * Deletes a entry from the Database
     * 
     * @param int $id ID of the entry
     * 
     * @return bool false if not sucessful, true if sucessful
     */
    public function deleteEntry(int $id)
    {
        $sql  = "DELETE FROM entry WHERE id=:id";

        $stmt = $this->db->prepare($sql);

        return $stmt->execute(['id' => $id]);
    }


    public function getNumberOfEntries():int {
        $sql = "SELECT COUNT(id) AS numb FROM entry";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return (int) $stmt->fetch(PDO::FETCH_ASSOC)['numb'];
    }
}
