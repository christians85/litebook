<?php

/*
 * In this project: 
 *    All folders are lower case.
 *    All ClassName are in file ClassName.class.php.
 *    The folder tree equals the namespace.
 */

namespace Litebook;

class Autoloader {
    protected $namespace = "";
    protected $base_dir = "";

    /**
     * Constructor for Autoloader.
     * @param string $namespace Namespace the project lives in.
     * @param string $base_dir Basic Directory of the project.
     */
    function __construct(string $namespace, string $base_dir) {
        $this->namespace = $namespace;
        $this->base_dir = $base_dir;
        $this->init();
    }

    /**
     * Get the value of namespace
     * @type string
     */ 
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * Set the value of namespace
     * @param string $namespace
     * @return \Autoloader
     */ 
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
        return $this;
    }

    /**
     * Get the value of base_dir
     * @type string
     */ 
    public function getBase_dir()
    {
        return $this->base_dir;
    }

    /**
     * Set the value of base_dir
     * @param string $base_dir
     * @return \Autoloader
     */ 
    public function setBase_dir($base_dir)
    {
        $this->base_dir = $base_dir;
        return $this;
    }

    /**
     * Initializes the autoloader for this project.
     */
    public function init() {
        spl_autoload_register(function ($class_name) {

            // Splits up the class name if '\' are used to define namespace.
            $class_name_parts = explode('\\', $class_name);
            // Only continues if the class is called by namespace.
            if (strcmp($class_name_parts[0], $this->namespace) != 0) {
                return true;
            }
        
            // Last element of the array is always the classname. In this
            // project it fits with the Filename, ending with .class.php
            $class_file_name = end($class_name_parts).".class.php";

            
            
            // Generates the path to class file out of namespace:
            $class_file_path = $this->base_dir;
            for($i = 1; $i < sizeof($class_name_parts)-1; $i++) {
                $class_file_path .= strtolower($class_name_parts[$i]) . DIRECTORY_SEPARATOR;
            }

            // Finally... requiring the file itself.
            require_once( $class_file_path . $class_file_name);
        });
    }    
}