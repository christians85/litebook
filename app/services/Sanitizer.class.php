<?php

namespace Litebook\Services;
use Litebook;
use PDO;

class Sanitizer {


    /**
     * Replaces HTML Chars by ASCII-Representation
     * 
     * @param string $input Input-String
     * 
     * @return string String without HTML Chars
     */
    public static function escape(string $input):string {
        return htmlspecialchars($input);
    }


    /**
     * Filters a variable with a specific filter.
     * 
     * @param string $input The name of the $_POST or $_GET ID.
     * @param int $filter Filter that is applied to string.
     * @param int $method INPUT_GET or INPUT_POST
     * 
     * @return mixed false if failure, sanitized string if sucessful
     */
    public static function filterInput(string $input, int $filter, int $method) {

        return filter_input($method, $input, $filter);

    }

}
