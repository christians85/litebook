<?php

namespace Litebook\Services;
use Litebook\Services\Sanitizer as Santizer;

class InputMerger {

    public static function merge():array {
        // Sanitizes POST Variables if set.
        $result = array();
        if (isset($_POST['e_email']))         $result['e_email']         = Santizer::filterInput('e_email', FILTER_SANITIZE_EMAIL, INPUT_POST);
        if (isset($_POST['e_homepage']))      $result['e_homepage']      = Santizer::filterInput('e_homepage', FILTER_SANITIZE_URL, INPUT_POST);
        if (isset($_POST['e_name']))          $result['e_name']          = Santizer::filterInput('e_name', FILTER_SANITIZE_EMAIL, INPUT_POST);
        if (isset($_POST['e_message']))       $result['e_message']       = Santizer::filterInput('e_message', FILTER_SANITIZE_EMAIL, INPUT_POST);
        if (isset($_GET['page']))             $result['page']            = Santizer::filterInput('page', FILTER_SANITIZE_NUMBER_INT, INPUT_GET);
        if (isset($_GET['email']))            $result['email']           = Santizer::filterInput('email', FILTER_SANITIZE_EMAIL, INPUT_GET);

        // Sanitzies all fields of GET and POST
        $result = filter_var_array(array_merge($result, $_GET), FILTER_SANITIZE_SPECIAL_CHARS);
        $result = filter_var_array(array_merge($result, $_POST), FILTER_SANITIZE_SPECIAL_CHARS);

        return $result;
    }

}