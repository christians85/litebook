<?php

namespace Litebook\Controllers;
use Litebook\Models\EntryModel as EntryModel;
use Litebook\Views\Templates as Templates;
use Litebook\Services\Sanitizer as Sanitizer;
use Litebook\Services\InputMerger as InputMerger;
use PDO;

define("DESC", 0);
define("ASC", 1);

class EntryController {

    private $db;
    private $model;

    public function __construct(PDO $db) {
         $this->db = $db;
         $this->model = new EntryModel($this->db);
    }


    private function checkValidInputs():bool {

        // if no Form is filled -> Name isn't set -> return false
        if(!isset($_POST['e_name'])) return false;

        // Check if honeypot for soambots is filled.
        if($_POST['email'] != "") die;
        if($_POST['homepage'] != "") die;

        // Check if all req. fields are filled up.
        if ($_POST['e_name'] == "") return false;
        if ($_POST['e_email'] == "") return false;
        if ($_POST['e_message'] == "") return false;

        // if nothing returned true -> the field was filled correctly.
        return true;

    }


    // TODO:
    private function isNoSpam():bool {
        return true;
    }

    public function post() {
        // Merging get and set
        $result = InputMerger::merge();
        
        // First Step: Check if the Form was filled correctly.
        if ($this->checkValidInputs() && $this->isNoSpam()) {

            
            $result = $this->model->addEntry(
                $result['e_name'],
                $result['e_email'],
                $result['e_homepage'],
                $result['e_message']
            );

            if($result) {
                echo "<div class=\"success\">Ihre Nachricht wurde erfolgreich gepostet.</div>";
                echo "<div class=\"info\">Wenn Sie nicht innerhalb der nächsten Sekunden weitergeleitet werden, klicken Sie bitte <a href=\"index.php\">hier</a>..</div>";
                ?>
                    <script>
                        setTimeout("self.location.href='index.php'",2500);
                    </script>
                <?php
            }

            return true;

        } 
        echo "Bitte füllen Sie das Formular mit allen erforderlichen Feldern aus.";
        return false;
    }

    /**
     * Realizes the public presentation of the guestbook entries with pagination.
     */
    public function publicPrsentation(int $page = 1, int $perpage = 10, int $direction = DESC) {
        // Vars for pagination:
        $offset = 0; 
        $maxpages = 0;

        // Inits pagination
        $this->initPagination($offset, $maxpages, $page, $perpage);

        $this->printEntries($offset, $perpage, 0);

        $this->printPagination($page, $maxpages);

    }

    /**
     * Inits the pagination for the gustbook.
     */
    private function initPagination(int &$offset, int &$maxpages, int &$page, int &$perpage) {
        // calculates the maximal number of pages
        $maxpages = (int) ceil($this->model->getNumberOfEntries() / $perpage);
        // there is no page < 1 and over the maximal possible number of pages
        $page = min(max($page, 1), $maxpages); 
        // calculates the offset where the current page begins
        $offset = (int) (($page-1) * $perpage);
        
    }

    /**
     * TODO: Neds a template
     * TODO: Needs ... notation for multiple pages
     * 
     * Prints pagination for the guestbook.
     */
    private function printPagination(int $page, int $maxpages) {

        echo "<ul class=\"pages\">";
        // Only shows "go page before" if there are pages before
        if($page > 1) {
            echo "<li><a href=\"?page=1\">&lt;&lt;</a></li>";
            echo "<li><a href=\"?page=" . ($page-1) . "\">&lt;</a></li>";
        }

        // Builds up a list of pages
        for($i = 1; $i <= $maxpages; $i++) {
            
            $class = ($i == $page) ? " class=\"current\"" : "";

            echo "<li" . $class . "><a href=\"?page=" . $i . "\">" . $i . "</a></li>";
        }

        // Only shows "go page beyond" if there are pages beyond
        if($page < $maxpages) {
            echo "<li><a href=\"?page=" . ($page+1) . "\">&gt;</a></li>";
            echo "<li><a href=\"?page=" . $maxpages . "\">&gt;&gt;</a></li>";
        }
        echo "</ul>";
        
    }


    /**
     * Prints a selected amount of entries beginning from an offset
     * 
     * @param int $offset The offset to begin with
     * @param int $number How many entries should be there?
     * @param int $number Direction (0 = descending - default) (1 = ascending)
     */
    public function printEntries (int $offset, int $number, int $direction = DESC) {
        $entries = $this->model->fromDBWithCondition($offset, $number, $direction);
        foreach ($entries as $entry) {
            Templates::place("pubview/gbentry", $entry);
        }
    }



}
