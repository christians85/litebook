<?php

namespace Litebook\Database;
use PDO;

require_once(__ROOT__ . "/app/configs/DB-CONSTANTS.php");

function getDB() {
    // only one instance of PDO-handler
    static $handler;

    // if PDO already was initialized, just return existing db Handler
    if($handler instanceof PDO) return $handler;

    // if there was no instance before, create one and return it
    $handler = new PDO(_DB_DSN_, _DB_USER_, _DB_PWD_);
    return $handler;
}