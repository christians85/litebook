A simple guestbook programmed foremost to learn some kind of MVC pattern in PHP, deal with autoload, simple templates and improve in PHP itself.  

Technologies I want to improve in
---------------------------------
PHP, SQL, JavaScript, PHP design pattern, SCSS, Typescript

Features I want to impelement
-----------------------------
Spam Protection  
Some Emotes placed by JavaScript  
Some BBCode like in forums  
Sanitizing inputs