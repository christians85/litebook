<?php

namespace Litebook;
use Litebook;
use Litebook\Controllers\EntryController as EntryController;

// Header Information

    // Constants
    require_once(__DIR__ . "/../app/configs/CONSTANTS.php");
    // Autoloader
    require_once(__DIR__ . "/../app/services/Autoloader.class.php");
    $loader = new Autoloader("Litebook", __ROOT__.DS."app".DS);
    // DB-Conn:
    require_once(__ROOT__ . "/app/Database.php");
    $db = Database\getDB();

    $inputs = Litebook\Services\InputMerger::merge();
    if(!isset($inputs['page'])) $inputs['page'] = 1;

// Displays the site itself
require_once(__DIR__ . "/../templates/header.php");

    $e_ctrl = new EntryController($db);

    $e_ctrl->post();

    


require_once(__ROOT__ . "/templates/pubview/nav.php");
require_once(__ROOT__ . "/templates/footer.php");