    <form action="post.php" method="post" class="pubform">
        <h3>Ihr Eintrag</h3>
        <div class="form-group hidden">
            <input type="email" name="email" id="email">
            <input type="url" name="homepage" id="homepage">
        </div>

        <div class="form-group">
            <label for="e_name">Name</label><span id="msg"></span>
            <input type="text" name="e_name" id="e_name" required>
        </div>
        <div class="form-group">
            <label for="e_email">E-Mail-Adresse</label><span id="msg"></span>
            <input type="email" name="e_email" id="e_email" required>
        </div>
        <div class="form-group">
            <label for="e_homepage">Homepage (optional) </label><span id="msg"></span>
            <input type="url" name="e_homepage" id="e_homepage">
        </div>
        <div class="form-group">
            <label for="e_message">Nachricht</label><span id="msg"></span>
            <textarea name="e_message" id="e_message" cols="60" rows="10" required></textarea>
        </div>
        <div class="form-group">
            <input type="submit" value="Eintrag posten">
            <input type="reset" value="Eingabe löschen">
        </div>
    </form>
