<?php

    namespace Litebook;
    use Litebook;

    require_once(__DIR__ . "/../app/configs/CONSTANTS.php");

?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>liteBook Alpha</title>
    <link rel="stylesheet" href="<?= __ROOT__ ?>/public/assets/css/main.css">
</head>
<body>
    <div class="wrapper">